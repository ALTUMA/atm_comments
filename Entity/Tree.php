<?php

namespace ATM\CommentBundle\Entity;


class Tree
{
    private $comment;

    private $children = array();


    public function __construct($comment = null)
    {
        $this->comment = $comment;
    }

    public function add($comment)
    {
        $this->children[$comment['id']] = new Tree($comment);
    }

    public function traverse($id)
    {
        return $this->children[$id];
    }

    public function toArray()
    {
        $children = array();
        foreach ($this->children as $child) {
            $children[] = $child->toArray();
        }

        return $this->comment ? array('comment' => $this->comment, 'children' => $children) : $children;
    }
}
