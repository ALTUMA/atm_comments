<?php

namespace ATM\CommentBundle\Entity;

use \DateTime;
use InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;

abstract class Comment {

    protected $id;

    protected $parent;

    /**
     * @ORM\Column(name="body", type="text", nullable=false, options={"collation": "utf8mb4_unicode_ci"})
     */
    protected $body;

    /**
     * @ORM\Column(name="depth", type="integer", nullable=false)
     */
    protected $depth = 0;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    protected $thread;

    /**
     * @ORM\Column(name="ancestors", type="text", nullable=true)
     */
    protected $ancestors;

    protected $author;


    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function __toString()
    {
        return 'Comment #'.$this->getId();
    }

    public function getDepth()
    {
        return $this->depth;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;

        if (!$parent->getId()) {
            throw new InvalidArgumentException('Parent comment must be persisted.');
        }

        $ancestors = $parent->getAncestors();
        $ancestors[] = $parent->getId();

        $this->setAncestors($ancestors);
    }

    public function getThread()
    {
        return $this->thread;
    }

    public function setThread($thread)
    {
        $this->thread = $thread;
    }

    public function getAncestors()
    {
        return $this->ancestors ? explode('/', $this->ancestors) : array();
    }

    public function setAncestors(array $ancestors)
    {
        $this->ancestors = implode('/', $ancestors);
        $this->depth = count($ancestors);
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function getAuthorName()
    {
        if (null === $this->getAuthor()) {
            return 'Anonymous';
        }

        return $this->getAuthor()->getUsername();
    }
}
