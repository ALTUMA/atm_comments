(function(window, $){
    var ATM_COMMENT = {

        initialize: function(){
            ATM_COMMENT.thread_container.on('submit','.atm_comment_comment_form',function(e){
                e.preventDefault();
                var form = $(this);
                var textarea = form.find('textarea');
                var body = textarea.val();
                var url = form.attr('action');
                var method = form.attr('method');
                var threadId = ATM_COMMENT.thread_container.attr('atm-thread-id');
                var parentCommentId = form.attr('data-atm-parent-comment-id');

                form.find('.atm_comment_submit input').attr("disabled", true);
                window.atm_comment_callbacks.onBeforeFormSubmit(ATM_COMMENT.thread_container);

                $.ajax({
                    url: url,
                    data: 'b='+encodeURIComponent(body)+'&ti='+threadId+'&pci='+parentCommentId,
                    type: method,
                    dataType: 'html',
                    success: function(data){
                        form.find('.atm_comment_submit input').removeAttr("disabled");
                        if(parentCommentId != ''){
                            form.parent().siblings('.atm_comment_comment_replies').append(data);
                        }else{
                            $('.atm_comment_container').prepend(data);
                        }
                        form.closest('.atm_comment_comment_reply_show_form').show();
                        form.parent().children('.atm_comment_comment_reply_show_form').show();
                        if(!form.parent().hasClass('atm_main_form_container')){
                            form.remove();
                        }
                        textarea.val('');
                        window.atm_comment_callbacks.onAfterFormSubmit(ATM_COMMENT.thread_container);
                    }
                });
            });

            ATM_COMMENT.thread_container.on('click','.atm_comment_comment_reply_show_form',function(e){
                e.preventDefault();
                var button = $(this);
                var parentCommentId = button.attr('data-parent-id');
                var url = button.attr('data-atm-url');
                var container = button.parent();

                window.atm_comment_callbacks.onBeforeShowReplyForm(ATM_COMMENT.thread_container,container);

                $.ajax({
                    url: url,
                    type: 'post',
                    data: 'pci=' + parentCommentId,
                    dataType: 'html',
                    success: function(data){
                        button.hide();
                        container.append(data);
                        window.atm_comment_callbacks.onAfterShowReplyForm(ATM_COMMENT.thread_container,container);
                    }
                });
            });

            ATM_COMMENT.thread_container.on('click','.atm_close_form',function(e){
                e.preventDefault();

                var button = $(this);
                var replyButton = button.parent().parent().siblings('.atm_comment_comment_reply_show_form');

                button.parent().parent().remove();
                replyButton.show();
            });

            ATM_COMMENT.thread_container.on('click','.atm_comment_delete_comment',function(e){
               e.preventDefault();

               var button = $(this);
               var url = button.attr('data-atm-url');
               var parentId = button.attr('data-atm-mainparent-id');

                window.atm_comment_callbacks.onBeforeDeleteComment(ATM_COMMENT.thread_container,button);

                $.ajax({
                   url: url,
                   dataType: 'json',
                   success: function(data){
                        $(parentId).remove();
                       window.atm_comment_callbacks.onAfterDeleteComment(ATM_COMMENT.thread_container,button,data);
                   }
               });

            });
        },
        getThreadComments: function(){
            window.atm_comment_callbacks.onBeforeGetComments(ATM_COMMENT.thread_container);
            $.ajax({
                url: window.atm_comment_thread_url,
                success: function(data){
                    ATM_COMMENT.thread_container.append(data);
                    window.atm_comment_callbacks.onAfterGetComments(ATM_COMMENT.thread_container);
                }
            })
        }

    };

    //set the main threat container if one is configured if not use the default one
    ATM_COMMENT.thread_container = window.atm_comment_thread_container || $('#atm_comment_thread');

    // set the appropriate base url
    //ATM_COMMENT.base_url = window.fos_comment_thread_api_base_url;

    if(typeof window.atm_comment_thread_id != "undefined") {
        // get the thread comments
        ATM_COMMENT.getThreadComments(window.atm_comment_thread_id);
    }

    ATM_COMMENT.initialize();
})(window, window.jQuery);