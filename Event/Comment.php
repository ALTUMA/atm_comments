<?php

namespace ATM\CommentBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class Comment extends Event
{
    const NAME = 'atm_comment.event';

    private $comment;
    private $entityType;
    private $entityId;

    public function __construct($comment)
    {
        $this->comment = $comment;
        $entityTokens = explode('_',$this->comment->getThread()->getId());
        $this->entityType = $entityTokens[0];
        $this->entityId = $entityTokens[1];
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function getEntityType(){
        return $this->entityType;
    }

    public function getEntityId(){
        return $this->entityId;
    }
}